from flask import Flask
import graphene
from graphene_sqlalchemy import SQLAlchemyObjectType, SQLAlchemyConnectionField
from flask_graphql import GraphQLView
from flask_graphql_auth import (
    AuthInfoField,
    GraphQLAuth,
    create_access_token,
    create_refresh_token,
    mutation_jwt_required,
)
from sqlalchemy.orm import relationship
from flask_sqlalchemy import SQLAlchemy


app = Flask(__name__)
app.config["SECRET_KEY"] = "secret"
app.config["JWT_SECRET_KEY"] = "secret"


POSTGRES = {
    "user": "postgres",
    "pw": "1234",
    "db": "flask",
    "host": "localhost",
    "port": "5432",
}


app.config["SQLALCHEMY_DATABASE_URI"] = (
    "postgresql://%(user)s:\
%(pw)s@%(host)s:%(port)s/%(db)s"
    % POSTGRES
)

app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False


db = SQLAlchemy(app)
auth = GraphQLAuth(app)


@app.route("/")
def home():
    return "home"


@app.teardown_appcontext
def shutdown_session(exception=None):
    db.session.remove()


class User(db.Model):
    __tablename__ = "users"
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(20), unique=True, nullable=False)
    password = db.Column(db.String(60), nullable=False)
    email = db.Column(db.String(100))
    stores = relationship("Store", backref="owner")


class Store(db.Model):
    __tablename__ = "stores"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(20), unique=True, nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey("users.id"))


class UserObject(SQLAlchemyObjectType):
    class Meta:
        model = User
        interfaces = (graphene.relay.Node,)


class StoreObject(SQLAlchemyObjectType):
    class Meta:
        model = Store
        interfaces = (graphene.relay.Node,)


class ProtectedStore(graphene.Union):
    class Meta:
        types = (StoreObject, AuthInfoField)


class CreateUser(graphene.Mutation):
    user = graphene.Field(UserObject)

    class Arguments:
        username = graphene.String(required=True)
        password = graphene.String(required=True)
        email = graphene.String(required=True)

    def mutate(self, info, username, password, email):
        user = User.query.filter_by(username=username).first()
        if user:
            return CreateUser(user=user)
        user = User(username=username, password=password, email=email)
        if user:
            db.session.add(user)
            db.session.commit()
        return CreateUser(user=user)


class CreateStore(graphene.Mutation):
    store = graphene.Field(ProtectedStore)

    class Arguments:
        name = graphene.String(required=True)
        user_id = graphene.Int(required=True)
        token = graphene.String()

    @mutation_jwt_required
    def mutate(self, info, name, user_id):
        store = Store(name=name, user_id=user_id)
        if store:
            db.session.add(store)
            db.session.commit()
        return CreateStore(store=store)


class Query(graphene.ObjectType):
    node = graphene.relay.Node.Field()
    all_users = SQLAlchemyConnectionField(UserObject)


class AuthMutation(graphene.Mutation):
    access_token = graphene.String()
    refresh_token = graphene.String()

    class Arguments:
        username = graphene.String()
        password = graphene.String()

    def mutate(self, info, username, password):
        user = User.query.filter_by(
            username=username, password=password
            ).first()
        print(user)
        if not user:
            raise Exception("Authenication Failure : User is not registered")
        return AuthMutation(
            access_token=create_access_token(username),
            refresh_token=create_refresh_token(username),
        )


class Mutation(graphene.ObjectType):
    create_user = CreateUser.Field()
    auth = AuthMutation.Field()
    protected_create_store = CreateStore.Field()


schema = graphene.Schema(query=Query, mutation=Mutation)

app.add_url_rule(
    "/graphql",
    view_func=GraphQLView.as_view("graphql", schema=schema, graphiql=True)
)


if __name__ == "__main__":
    app.run(debug=True)
